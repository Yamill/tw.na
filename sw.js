self.addEventListener('install', function (event) {
    console.log('[ServiceWorker] Installing Service Worker ...', event);
    event.waitUntil(
        caches.open('static')
        .then(function(cache) {
            // console.log('[Service Worker] Precaching app shell')
            // cache.addAll([
            //     '/',
            //     '/home',
            //     '/js/app.js',
            //     'app.js',
            //     '/css/owl/owl.carousel.min.css',
            //     '/css/owl/owl.theme.default.min.css',
            //     'manifest.json',
            //     '/css/main.css',
            //     '/js/modernizr/modernizr.min.js',
            //     '/js/jquery/jquery-2.2.4.min.js',
            //     '/js/bootstrap/bootstrap.min.js',
            //     '/js/revslider/jquery.themepunch.tools.min.js',
            //     '/js/revslider/jquery.themepunch.revolution.min.js',
            //     '/js/animsition/animsition.min.js',
            //     '/js/main.min.js',
            //     '/css/main.min.css',
            //     '/css/revslider/settings.css',
            //     '/css/font-awesome/font-awesome.min.css',
            //     '/css/ionicons.min.css',
            //     '/css/animate.min.css',
            //     '/css/animsition.min.css',
            //     '/css/bootstrap.min.css',
            //     '/css/revslider/layers.css'
            // ]);
            cache.add('/')
            cache.add('/home')
            cache.add('/js/app.js')
            cache.add('/css/owl/owl.carousel.min.css')
            cache.add('/css/owl/owl.theme.default.min.css')
            cache.add('manifest.json')
            cache.add('/css/main.css')
            cache.add('/js/modernizr/modernizr.min.js')
            cache.add('/js/jquery/jquery-2.2.4.min.js')
            cache.add('/js/bootstrap/bootstrap.min.js')
            cache.add('/js/revslider/jquery.themepunch.tools.min.js')
            cache.add('/js/revslider/jquery.themepunch.revolution.min.js')
            cache.add('/js/animsition/animsition.min.js')
            cache.add('/js/main.min.js')
            cache.add('/css/main.min.css')
            cache.add('/css/revslider/settings.css')
            cache.add('/css/font-awesome/font-awesome.min.css')
            cache.add('/css/ionicons.min.css')
            cache.add('/css/animate.min.css')
            cache.add('/css/animsition.min.css')
            cache.add('/css/bootstrap.min.css')
            cache.add('/css/revslider/layers.css')
            cache.add('/images/logo.png')
            cache.add('/images/slider/tp_vid_clouds-1.jpg')
            cache.add('https://fonts.googleapis.com/css?family=Poppins:600|Questrial')
            cache.add('/js/jquery.waypoints.min.js')
            cache.add('/js/jquery.counterup.min.js')
            cache.add('/js/parallax.min.js')
            cache.add('/js/owl.carousel.min.js')
            cache.add('/js/slider.min.js')
            cache.add('/js/slider.min.js')
        })
        )
});

self.addEventListener('activate', function (event) {
    // console.log('[ServiceWorker] Activating Service Worker ...', event);
    return self.clients.claim();
});

self.addEventListener('fetch', function(event) {
    // console.log('[ServiceWorker] Fetching something ...', event);
    event.respondWith(
        caches.match(event.request)
        .then(function(response) {
            if (response) {
                return response;
            } else {
                return fetch(event.request)
                    .then(function(res) {
                        return caches.open('dynamic')
                            .then(function(cache) {
                                cache.put(event.request.url, res.clone);
                                return res;
                            })
                    });
            }
        })
    );
});